# Demo UChat Design

## API Design

### Login
* url: http://{ServerAddr}/Login/{username}
* methods: 
	* POST: login
	* Others: Invalid

### Logout
* url: http://{ServerAddr}/Logout/{username}
* methods: 
	* POST: logout
	* Others: invalid

### Group Chat
* url: http://{ServerAddr}/GroupChat
* methods:
	* POST: create
	* Others: invalid

* url: http://{ServerAddr}/GroupChat/id
* methods:
	* PUT: join
	* Others invalid

###

### Chat (WebSocket)
* url: ws://{ServerAddr}/Chat

## Data Structure Design

* Message : 
	* from : {string}
	* to : {string}
	* Tag: {enum [person, group]}
	* type : {enum [text, pic]}
	* timestamp : {int}
	* content : {string}
	  

* User :
	* name : {string}
	* icon : {pic}
	* friends : {array(string)}

