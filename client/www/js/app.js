function start () {
        $(function() {
            // Show default wechat tab content.
             showTab('wechat');
             bindNaviTabEvents();
            
            console.log("on start");
            var friends = ['barrett', 'slimer', 'spengler', 'stantz', 'tully', 'venkman', 'winston'];
            friends.sort();
            var nameMap = {};
            for (var i = 0; i < 26; i++) {
                nameMap[String.fromCharCode(65 + i)] = [];
            }
            for (i = 0; i < friends.length; i++) {
                nameMap[friends[i][0].toLocaleUpperCase()].push(friends[i]);
            }
            
            for (i = 0; i < 26; i++) {
                var ch = String.fromCharCode(65 + i);
                if (nameMap[ch].length !== 0) {
                    var dividerHTML = '<div class="item item-divider">' + ch + '</div>';
                    $("#contact-list").append(dividerHTML);
                    for (var j = 0; j < nameMap[ch].length; j++) {
                        contactHTML = '<a class="item" href="#">' + nameMap[ch][j] + '</a>';
                        $("#contact-list").append(contactHTML);
                    }
                }
            }
        });
    }

function showTab (tabId) {
	// Hide all content tab.
	$('.content').hide();
	// Show the specified content.
	$('#' + tabId + '-content').show();
}

function bindNaviTabEvents () {
	
	$('.tab-item').click(function () {
		var tabId = $(this).attr('id');
		showTab(tabId);
	});
}

start();